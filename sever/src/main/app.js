// const createError = require('http-errors');
const express = require('express');
const path = require('path');
// const cookieParser = require('cookie-parser');
// const logger = require('morgan');
//const cors = require('cors');

// const indexSecuredAPIRouter = require('../secured-api/routes');
const indexOpenAPIRouter = require('../open_api/routes');

const app = express();

//app.use(cors());
//app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
//app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// const authorizations = [
//   require('../secured-api/middlewares/authorization/user_session'),
// ]

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Credentials', true);
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
  res.setHeader('Cache-Control', 'private, no-cache, no-store, must-revalidate');
  res.setHeader('Expires', '-1');
  res.setHeader('Pragma', 'no-cache');
  next();
});


//app.all('/secured-api/*',authorizations);
app.use('/', indexOpenAPIRouter);
//app.use('/', indexSecuredAPIRouter);

module.exports = app;
