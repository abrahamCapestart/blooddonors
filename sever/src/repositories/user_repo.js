const psql_client = require('../db/postgres');

const UsersService = {
  getAllUsers() {
        return psql_client.select("*").from("users");
  },
  insertUser(newUser) {
      return psql_client
      .insert(newUser)
      .into("users")
      .returning("*")
      .then((rows) => {
        return rows[0];
      });
  },
  getById(id) {
      return psql_client.from("users").select("*").where("id", id).first();
  },
  deleteUser(id) {
      return psql_client("users").where({ id }).delete();
  },
  updateUser(id, userFields) {
      return psql_client("users").where({ id }).update(userFields);
  },
};

module.exports = UsersService;