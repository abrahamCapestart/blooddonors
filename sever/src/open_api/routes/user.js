var ctrlUser = require('../controllers/user_controller');

module.exports = function (router) {
    console.log('User Details APIs');
    router
        .route('/api/adduser')
        .post(ctrlUser.userdetails);
    router
        .route('/api/allusers')
        .get(ctrlUser.userlist);
    router
        .route('/api/editsigleitem/:id')
        .post(ctrlUser.edititem);
    router
        .route('/api/deletesingleitem/:id')
        .delete(ctrlUser.deleteitem);
    
    return router;
}
