const userRepo = require('../../repositories/user_repo');

module.exports.userlist = function(req, res) {
    userRepo.getAllUsers().then(data => {
        res.json(data);
            });
}

module.exports.userdetails = function (req, res) {
    userRepo.insertUser(req.body).then(data => {
        res.json(data);
    });
}

module.exports.edititem = function (req, res) {
    userRepo.updateUser(db, req.params.id, req.body).then(() => {
        res.status(204).end();
    });
}

module.exports.deleteitem = function (req, res) {
    userRepo.deleteUser(db, req.params.id).then(data => {
        res.status(204).end();
    });
}
