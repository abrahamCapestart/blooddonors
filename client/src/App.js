import React, { Component } from 'react';
import { BrowserRouter as Router, Link, NavLink, Prompt } from 'react-router-dom';
import Route from 'react-router-dom/Route';

class App extends Component {

    state = {
        logged_in: false
    }

    loginHandle = () => {
        this.setState(prevState => ({
            logged_in: !prevState.logged_in
        }))
    }

    render() {
        return (
            <Router>
                <div className="App">
                    <ul>
                        <li>
                            <Link to="/" exact>Website Home page</Link>
                        </li>
                        <li>
                            <Link to="/about" exact>About</Link>
                        </li>
                        <li>
                            <Link to="/home/Abraham" exact>Application Home</Link>
                        </li>
                    </ul>

                    <Route path="/about" exact render={
                        () => {
                            return (<h1>This is about</h1>);
                        }
                    } />
                    <Route path="/" exact render={
                        () => {
                            return (<h1>Welcome Home</h1>);
                        }
                    } />
                    <Route path="/home/:name" exact render={
                        ({ match }) => {
                            return (<div>
                                <h1>Hi {match.params.name}</h1>
                                <h1><input type="text" value={this.state.logged_in}></input></h1>
                                <input type="button" value={this.state.logged_in ? 'Logout' : 'Login'} onClick={this.loginHandle.bind(this)} />
                                <Prompt
                                    when={!this.state.logged_in}
                                    message={(location) => {
                                        return 'Please Login'
                                    }}
                                />
                            </div>
                            );
                        }

                    }/>
                </div>
            </Router>
        );

    }
}

export default App;